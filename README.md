# Paragraphs - czyli jak zapanować nad rozbudowaną strukturą treści
 
&copy; Michał Borek @ Drupal Camp Poland 2018

## Informacje postawowe

* https://www.drupalvm.com
* https://github.com/drupal-composer/drupal-project
* https://www.drupal.org/project/paragraphs

## Instalacja

* wymagany _Vagrant_ z pluginami `vagrant-hostsupdater`, `vagrant-vbguest`, zalecany także `vagrant-autonetwork`
* skopiować plik `default.config.yml` do `config.yml`
* dostosować konfigurację środowiska wirtualnego w `config.yml`
* polecenie `vagrant up` utworzy i uruchomi wirtualną maszynę

Otrzymujemy pełne środowisko wirtualne z:
* dashboardem maszyny `http://dashboard.drupalvm.test`
* zainstalowanym Drupalem `http://drupalvm.test`

## Odtworzenie backupu bazy danych

...